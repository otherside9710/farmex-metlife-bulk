<?php

/** @var \Laravel\Lumen\Routing\Router $router */

use Illuminate\Http\Request;
use App\Http\Controllers\user\GetNegotiatedPriceController;
use App\Http\Controllers\product\DeleteProductsController;
use App\Http\Controllers\checkout\DeleteProductsAbandonedCheckoutsController;
use App\Exceptions\Shared\GenericHTTPExceptions;
use App\Http\Controllers\product\GetProductsController;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->post('/getNewProductIdByProduct', ['middleware' => 'cors', function (Request $request) use ($router) {
    if (empty($request->products)) {
        $genericHTTPExceptions = new GenericHTTPExceptions();
        $genericHTTPExceptions->NOT_FOUND('The Data is Empty');
    }

    $getProductsController = new GetProductsController();
    return $getProductsController->getNewProductIdByProduct($request->products);
}]);

$router->post('/getSkuProductByProductId', ['middleware' => 'cors', function (Request $request) use ($router) {
    if (empty($request->product_id)) {
        $genericHTTPExceptions = new GenericHTTPExceptions();
        $genericHTTPExceptions->NOT_FOUND('The Data is Empty');
    }

    $getProductsController = new GetProductsController();
    return $getProductsController->getSkuProductByProductId($request->product_id);
}]);

$router->post('/getNegotiatedPrice', ['middleware' => 'cors', function (Request $request) use ($router) {
    if (empty($request->farmexData)) {
        $genericHTTPExceptions = new GenericHTTPExceptions();
        $genericHTTPExceptions->NOT_FOUND('The Data is Empty');
    }

    $getNegotiatedPriceController = new GetNegotiatedPriceController();
    return $getNegotiatedPriceController->getNegotiatedPrice($request->farmexData);
}]);

$router->post('/deleteProducts', function (Request $request) use ($router) {

    if (empty($request)) {
        $genericHTTPExceptions = new GenericHTTPExceptions();
        $genericHTTPExceptions->NOT_FOUND('The Data is Empty');
    }

    $deleteProductsController = new DeleteProductsController();
    return $deleteProductsController->getAndDeleteProductByTags($request);

});

$router->post('/deleteAbandonedCheckout', function () use ($router) {
    $deleteProductsAbandonedCheckoutsController = new DeleteProductsAbandonedCheckoutsController();
    return $deleteProductsAbandonedCheckoutsController->DeleteProductsAbandonedCheckouts();
});
