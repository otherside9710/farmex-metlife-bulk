<?php

namespace App\domain\user;

use SoapClient;
use SoapFault;
use SoapHeader;
use App\Exceptions\Shared\GenericHTTPExceptions;

class SoapService
{
    private $uri;
    private $base_uri;
    private $soap;

    public function __construct()
    {
        $this->uri = env('FARMEX_WSDL_URI');
        $this->base_uri = env('FARMEX_WSDL_BASE_URI');
        $this->auth_user = env('FARMEX_WSDL_AUTH_USER');
        $this->auth_password = env('FARMEX_WSDL_AUTH_PASSWORD');

        $this->soap = new SoapClient($this->uri, [
            # Helps with debugging
            'trace' => TRUE,
            'exceptions' => TRUE
        ]);

        $auth = array(
            'UserName' => $this->auth_user,
            'Password' => $this->auth_password
        );

        $headers = new SoapHeader($this->base_uri, 'authentification', $auth, false);

        $this->soap->__setSoapHeaders($headers);
    }

    public function getNegotiatedPriceSOAP($productsData)
    {
        try {

            $input = array(
                'rut' => $productsData["rut"],
                'lstProductosInput' => array(
                    'InputProductosNegociados' => $productsData["products"]
                )
            );

            $response = $this->soap->obtenerInformacionCertificacion($input);

            $dataDeconstruct = $this->deconstruct($response);

            $shopifyService = new ShopifyService();
            $shopifyService->createProducts($dataDeconstruct[0], $dataDeconstruct[1]);
        } catch (SoapFault $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->INTERNAL_SERVER_ERROR(json_encode($e), $e->getMessage());
        }
    }


    private function deconstruct($object)
    {
        return[
            $object->obtenerInformacionCertificacionResult->lstOutProductos->OutputProductosNegociados,
            $this->getCampaign($object)
        ];
    }

    private function getCampaign($object){
        return $object->obtenerInformacionCertificacionResult->campania;
    }

}
