<?php


namespace App\domain\user;

use App\Exceptions\Shared\GenericHTTPExceptions;

ini_set('display_errors', 1);

class ShopifyService
{

    private $api_key;
    private $api_password;
    private $api_admin_path;
    private $metlife_shopify_host_server;
    private $metlife_shopify_api_admin;
    private $product_discount_identifier;
    private $metlife_shopify_api_admin_checkout;
    private $created_at_min_abandoned_checkouts;
    private $created_at_max_abandoned_checkouts;

    public function __construct()
    {
        $this->api_key = env('FARMEX_METLIFE_SHOPIFY_API_KEY');
        $this->api_password = env('FARMEX_METLIFE_SHOPIFY_API_PASSWORD');
        $this->api_admin_path = env('FARMEX_METLIFE_SHOPIFY_API_ADMIN_PATH');
        $this->metlife_shopify_host_server = env('FARMEX_METLIFE_SHOPIFY_HOST_SERVER');
        $this->metlife_shopify_api_admin = env('FARMEX_METLIFE_SHOPIFY_API_ADMIN_URL');
        $this->metlife_shopify_api_admin_checkout = env('FARMEX_METLIFE_SHOPIFY_API_ADMIN_URL_CHECKOUT');
        $this->product_discount_identifier = env('FARMEX_METLIFE_SHOPIFY_PRODUCT_DISCOUNT_IDENTIFIER');
        $this->created_at_min_abandoned_checkouts = env('FARMEX_CONSTANT_CREATED_AT_MIN_ABANDONED_CHECOUTS');
        $this->created_at_max_abandoned_checkouts = env('FARMEX_CONSTANT_CREATED_AT_MAX_ABANDONED_CHECOUTS');
    }

    public function createProducts($products_negotiated, $campaign)
    {
        $this->getDataOfProductAndBuild($products_negotiated, $campaign);
    }

    private function getDataOfProductAndBuild($products_negotiated, $campaign)
    {
        if (is_array($products_negotiated)) {
            $products_new_ids = array();
            foreach ($products_negotiated as $pr) {
                if ($pr->isClientMetlife == true) {
                    $product_id = $pr->idProducto;
                    $product_info = json_decode(file_get_contents($this->metlife_shopify_api_admin . $product_id . ".json"));

                    if ($pr->cantidad > $product_info->product->variants[0]->inventory_quantity) {
                        $dataError = [
                            'quantityExceeded' => true,
                            'nameProduct' => $product_info->product->title
                        ];

                        $genericHTTPExceptions = new GenericHTTPExceptions();
                        $genericHTTPExceptions->INTERNAL_SERVER_ERROR_CUSTOM([], "Quantity exceeded", $dataError);
                    }

                    $priceNegotiated = $pr->precioNegociado == 0 ? $product_info->product->variants[0]->price : $pr->precioNegociado;

                    if ($pr->isCertificacion == true) {
                        $priceNegotiated = $pr->certificacion->Certificacion->ReembolsoCompania;
                    }

                    $newProductBuild = $this->buildProduct($product_info, $priceNegotiated);

                    $products_new_ids[] = [
                        "id" => $newProductBuild["id_variant"],
                        "originalPrice" => $product_info->product->variants[0]->price,
                        "campaign" => $campaign,
                        "id_original" => $product_id,
                        "id_product" => $newProductBuild["id_product"],
                        "name_product" => $newProductBuild["name_product"],
                        "product" => $pr,
                        "flag" => 'yes-array'
                    ];

                    /* try {
                         $this->deleteMetafieldsProductById($newProductBuild["id_variant"]);
                     }catch (\Exception $e){}
                    */
                } else {
                    $product_id = $pr->idProducto;
                    $product_info = json_decode(file_get_contents($this->metlife_shopify_api_admin . $product_id . ".json"));

                    if ($pr->cantidad > $product_info->product->variants[0]->inventory_quantity) {
                        $dataError = [
                            'quantityExceeded' => true,
                            'nameProduct' => $product_info->product->title
                        ];

                        $genericHTTPExceptions = new GenericHTTPExceptions();
                        $genericHTTPExceptions->INTERNAL_SERVER_ERROR_CUSTOM([], "Quantity exceeded", $dataError);
                    }

                    $priceNegotiated = $product_info->product->variants[0]->compare_at_price == null ? $product_info->product->variants[0]->price : $product_info->product->variants[0]->compare_at_price;

                    $newProductBuild = $this->buildProduct($product_info, $priceNegotiated);

                    $products_new_ids[] = [
                        "id" => $newProductBuild["id_variant"],
                        "originalPrice" => $product_info->product->variants[0]->price,
                        "campaign" => $campaign,
                        "id_original" => $product_id,
                        "id_product" => $newProductBuild["id_product"],
                        "name_product" => $newProductBuild["name_product"],
                        "product" => $pr,
                        "flag" => 'yes-array'
                    ];
                }
            }
            echo json_encode($products_new_ids);
        } else {
            if ($products_negotiated->isClientMetlife == true) {
                $products_new_ids = array();
                $product_id = $products_negotiated->idProducto;
                $product_info = json_decode(file_get_contents($this->metlife_shopify_api_admin . $product_id . ".json"));

                if ($products_negotiated->cantidad > $product_info->product->variants[0]->inventory_quantity) {
                    $dataError = [
                        'quantityExceeded' => true,
                        'nameProduct' => $product_info->product->title
                    ];

                    $genericHTTPExceptions = new GenericHTTPExceptions();
                    $genericHTTPExceptions->INTERNAL_SERVER_ERROR_CUSTOM([], "Quantity exceeded", $dataError);
                }

                $priceNegotiated = $products_negotiated->precioNegociado == 0 ? $product_info->product->variants[0]->price : $products_negotiated->precioNegociado;

                if ($products_negotiated->isCertificacion == true) {
                    $priceNegotiated = $products_negotiated->certificacion->Certificacion->ReembolsoCompania;
                }

                $newProductBuild = $this->buildProduct($product_info, $priceNegotiated);

                $products_new_ids[] = [
                    "id" => $newProductBuild["id_variant"],
                    "originalPrice" => $product_info->product->variants[0]->price,
                    "campaign" => $campaign,
                    "id_original" => $product_id,
                    "id_product" => $newProductBuild["id_product"],
                    "name_product" => $newProductBuild["name_product"],
                    "product" => $products_negotiated,
                    "flag" => 'no-array'
                ];

                /*try {
                    $this->deleteMetafieldsProductById($newProductBuild["id_variant"]);
                }catch (\Exception $e){}
                */
            } else {
                $products_new_ids = array();
                $product_id = $products_negotiated->idProducto;
                $product_info = json_decode(file_get_contents($this->metlife_shopify_api_admin . $product_id . ".json"));

                if ($products_negotiated->cantidad > $product_info->product->variants[0]->inventory_quantity) {
                    $dataError = [
                        'quantityExceeded' => true,
                        'nameProduct' => $product_info->product->title
                    ];

                    $genericHTTPExceptions = new GenericHTTPExceptions();
                    $genericHTTPExceptions->INTERNAL_SERVER_ERROR_CUSTOM([], "Quantity exceeded", $dataError);
                }

                $priceNegotiated = $product_info->product->variants[0]->compare_at_price == null ? $product_info->product->variants[0]->price : $product_info->product->variants[0]->compare_at_price;

                $newProductBuild = $this->buildProduct($product_info, $priceNegotiated);

                $products_new_ids[] = [
                    "id" => $newProductBuild["id_variant"],
                    "originalPrice" => $product_info->product->variants[0]->price,
                    "campaign" => $campaign,
                    "id_original" => $product_id,
                    "id_product" => $newProductBuild["id_product"],
                    "name_product" => $newProductBuild["name_product"],
                    "product" => $products_negotiated,
                    "flag" => 'no-array'
                ];

                /*try {
                    $this->deleteMetafieldsProductById($newProductBuild["id_variant"]);
                }catch (\Exception $e){}
                */

            }
            echo json_encode($products_new_ids);
        }
    }

    private function buildProduct($product, $priceNegotiated)
    {
        $image = $product->product->images[0]->src;
        $id_product = $product->product->id;

        if (strpos($product->product->tags, ",")) {
            $tags = explode(",", preg_replace('/\s+/', '', $product->product->tags));
            if (in_array("delete", $tags, true)) {
                $id_product = $tags[0];
            }
        }

        $product_new = array('title' => $product->product->title . $this->product_discount_identifier,
            'body_html' => $product->product->body_html,
            'product_type' => $product->product->product_type,
            'published' => true,
            'tags' => "delete," . $id_product,
            'variants' => array(
                array('option1' => 'Default',
                    'price' => $priceNegotiated,//TODO: question this.
                    'sku' => $product->product->variants[0]->sku,
                    'inventory_quantity' => $product->product->variants[0]->inventory_quantity,
                    'inventory_management' => 'shopify',
                    'taxable' => true,
                    'compare_at_price' => $product->product->variants[0]->price,
                    'requires_shipping' => true
                )
            ),
            'images' => array(
                array('src' => $image)
            )
        );

        return $this->putProduct($product_new);
    }

    private function putProduct($product)
    {
        try {
            $base_url_api_shopify = "https://" . $this->api_key . ":" . $this->api_password . $this->metlife_shopify_host_server;

            $curl_client = curl_init($base_url_api_shopify . $this->api_admin_path); //set the url
            $product_json = json_encode(array('product' => $product)); //encode the product as json
            curl_setopt($curl_client, CURLOPT_CUSTOMREQUEST, "POST");  //specify this as a POST
            curl_setopt($curl_client, CURLOPT_POSTFIELDS, $product_json); //set the POST json
            curl_setopt($curl_client, CURLOPT_RETURNTRANSFER, true); //specify return value as string
            curl_setopt($curl_client, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json',
                    'Content-Length: ' . strlen($product_json))
            ); //specify that this is a JSON call
            $server_output = curl_exec($curl_client); //get server output if you wish to error handle / debug
            $variable = json_decode($server_output);
            curl_close($curl_client); //close the connection

            if (isset($variable->errors)) {
                $genericHTTPExceptions = new GenericHTTPExceptions();
                $genericHTTPExceptions->INTERNAL_SERVER_ERROR_CUSTOM($variable->errors, "error en la respuesta de la api", $product);
            }

            return [
                'id_variant' => $variable->product->variants[0]->id,
                'id_product' => $variable->product->id,
                'name_product' => $variable->product->title,
            ];
        } catch (\Exception $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->INTERNAL_SERVER_ERROR(json_encode($e), $e->getMessage());
        }
    }

    public function getNewProductIdByProduct($products_ids_list)
    {
        if (is_array($products_ids_list)) {
            $newsID = array();
            foreach ($products_ids_list as $product) {
                $product_info = $this->getProductByIdHandleBlankError($product['idProducto']);
                if (!empty($product_info)) {
                    if (strpos($product_info->product->tags, ",")) {
                        $tags = explode(",", preg_replace('/\s+/', '', $product_info->product->tags));
                        if (in_array("delete", $tags, true)) {
                            $newsID[] = [
                                "id_original" => $tags[0],
                                "id_new" => $product['idProducto'],
                            ];
                        } else {
                            $newsID[] = [
                                "id_original" => $product['idProducto'],
                                "id_new" => $product['idProducto'],
                            ];
                        }
                    } else {
                        $newsID[] = [
                            "id_original" => $product['idProducto'], //TODO: algunos productos tienen tag, entonces hay que buscar el que sea numero(proudct ID))
                            "id_new" => $product['idProducto'],
                        ];
                    }
                }
            }
            echo json_encode($newsID);
        } else {
            $newsID = array();
            $product_info = $this->getProductByIdHandleBlankError($products_ids_list['idProducto']);
            if (!empty($product_info)) {
                if (strpos($product_info->product->tags, ",")) {
                    $tags = explode(",", preg_replace('/\s+/', '', $product_info->product->tags));
                    if (in_array("delete", $tags, true)) {
                        $newsID[] = [
                            "id_original" => $tags[0], //TODO: algunos productos tienen tag, entonces hay que buscar el que sea numero(proudct ID))
                            "id_new" => $products_ids_list['idProducto'],
                        ];
                    }
                }
            }
            echo json_encode($newsID);
        }
    }

    function getProductByIdHandleBlankError($product_id)
    {
        try {
            return json_decode(file_get_contents($this->metlife_shopify_api_admin . $product_id . ".json"));
        } catch (\Exception $e) {
            return '';
        }
    }

    public function getAndDeleteProductByTags($products)
    {
        try {
            if (is_array($products)) {
                foreach ($products as $product){
                    foreach ($product["line_items"] as $pr) {
                        $product_info = $this->getProductById($pr->product_id);

                        if (!empty($product_info->product->tags)){
                            if (strpos($product_info->product->tags, ",")) {
                                $tags = explode(",", preg_replace('/\s+/', '', $product_info->product->tags));
                                if (in_array("delete", $tags, true)) {
                                    $this->deleteProductById($pr->product_id);
                                }
                            }else if ($product_info->product->tags == "delete"){
                                $this->deleteProductById($pr->product_id);
                            }
                        }
                    }
                }
            } else {
                $pr = (object)$products->line_items;

                $product_info = $this->getProductById($pr->product_id);

                if (!empty($product_info->product->tags)) {
                    if (strpos($product_info->product->tags, ",")) {
                        $tags = explode(",", preg_replace('/\s+/', '', $product_info->product->tags));
                        if (in_array("delete", $tags, true)) {
                            $this->deleteProductById($pr->product_id);
                        }
                    } else if ($product_info->product->tags == "delete") {
                        $this->deleteProductById($pr->product_id);
                    }
                }

            }
        } catch (\Exception $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->INTERNAL_SERVER_ERROR(json_encode($e), $e->getMessage());
        }
    }


    private function deleteProductById($product_id)
    {
        try {

            $curl_client = curl_init($this->metlife_shopify_api_admin . $product_id . ".json"); //set the url

            curl_setopt($curl_client, CURLOPT_CUSTOMREQUEST, "DELETE");  //specify this as a POST
            curl_setopt($curl_client, CURLOPT_RETURNTRANSFER, true); //specify return value as string
            curl_setopt($curl_client, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json'
                )
            ); //specify that this is a JSON call
            $server_output = curl_exec($curl_client); //get server output if you wish to error handle / debug
            curl_close($curl_client); //close the connection

        } catch (\Exception $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->INTERNAL_SERVER_ERROR(json_encode($e), $e->getMessage());
        }
    }

    private function getProductById($product_id)
    {
        try {
            return json_decode(file_get_contents($this->metlife_shopify_api_admin . $product_id . ".json"));
        } catch (\Exception $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->NOT_FOUND("el producto con id " . $product_id . " no existe");
        }
    }

    public function deleteAbandonedCheckouts()
    {
        try {
            $products = $this->getAbandonedCheckouts();
            if (!empty($products)) {
                $this->getAndDeleteProductByTags($products);
            }
        } catch (\Exception $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->NOT_FOUND("error intentando borrar los pruductos del checkout o no hay carritos abandonados");
        }
    }

    public function getAbandonedCheckouts()
    {
        try {
            $created_at_min = date("Y-m-d:H:i-s", strtotime(date("Y-m-d H:i:s") . "- " . (
                    $this->created_at_min_abandoned_checkouts + $this->created_at_max_abandoned_checkouts
                ) . " days"));
            $created_at_max = date("Y-m-d:H:i-s", strtotime(date("Y-m-d H:i:s") . "- " . $this->created_at_min_abandoned_checkouts . " days"));

            $data = array('created_at_min' => $created_at_min, 'created_at_max' => $created_at_max);

            $options = array(
                'https' => array(
                    'method' => 'GET',
                    'content' => http_build_query($data))
            );

            $stream = stream_context_create($options);

            try {
                $checkouts = json_decode(file_get_contents($this->metlife_shopify_api_admin_checkout . ".json", false, $stream));
            } catch (\Exception $e) {
                $genericHTTPExceptions = new GenericHTTPExceptions();
                $genericHTTPExceptions->NOT_FOUND("No hay checkouts para borrar. " . $e);
            }
            $line_items = array();

            foreach ($checkouts->checkouts as $checkout) {
                $line_items[] = [
                    "line_items" => $checkout->line_items
                ];
            }
            return $line_items;
        } catch (\Exception $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->INTERNAL_SERVER_ERROR($e->getTraceAsString(), $e->getMessage());
        }
    }

    private function deleteMetafieldsProductById($product_id)
    {
        try {

            $metafield = array('namespace' => "seo",
                'key' => "hidden",
                'value' => 1,
                'published' => true,
                'value_type' => 'integer'
            );

            $curl_client = curl_init($this->metlife_shopify_api_admin . $product_id . "/metafields.json"); //set the url
            $product_json = json_encode(array('metafield' => $metafield)); //encode the metafield as json
            curl_setopt($curl_client, CURLOPT_CUSTOMREQUEST, "POST");  //specify this as a POST
            curl_setopt($curl_client, CURLOPT_RETURNTRANSFER, true); //specify return value as string
            curl_setopt($curl_client, CURLOPT_POSTFIELDS, $product_json); //set the POST json
            curl_setopt($curl_client, CURLOPT_HTTPHEADER, array(
                    'Content-Type: application/json'
                )
            ); //specify that this is a JSON call
            $server_output = curl_exec($curl_client);
            echo($server_output);
            curl_close($curl_client); //close the connection

        } catch (\Exception $e) {
            $genericHTTPExceptions = new GenericHTTPExceptions();
            $genericHTTPExceptions->INTERNAL_SERVER_ERROR(json_encode($e), $e->getMessage());
        }
    }


    public function getSkuProductByProductId($product_id)
    {
        $productInfo = json_decode(file_get_contents($this->metlife_shopify_api_admin . $product_id . ".json"));
        echo json_encode([
            "sku" => $productInfo->product->variants[0]->sku
        ]);
    }
}
