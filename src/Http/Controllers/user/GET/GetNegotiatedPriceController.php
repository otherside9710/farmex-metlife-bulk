<?php


namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\domain\user\NegotiatedPrice;

class GetNegotiatedPriceController extends Controller
{
    public function getNegotiatedPrice($productsData){
        $negotiatedPrice = new NegotiatedPrice();
        $negotiatedPrice->getNegotiatedPrice($productsData);
    }
}
