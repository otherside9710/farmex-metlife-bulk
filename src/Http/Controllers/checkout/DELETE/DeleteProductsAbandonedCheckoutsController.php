<?php


namespace App\Http\Controllers\checkout;

use App\domain\checkout\AbandonedCheckouts;
use App\Http\Controllers\Controller;

class DeleteProductsAbandonedCheckoutsController extends Controller
{

    public function DeleteProductsAbandonedCheckouts(){
        $abandonedCheckouts = new AbandonedCheckouts();
        $abandonedCheckouts->abandonedCheckouts();
    }
}
