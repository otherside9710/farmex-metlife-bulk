<?php


namespace App\Http\Controllers\product;

use App\Http\Controllers\Controller;
use App\domain\product\DeleteProduct;

class DeleteProductsController extends Controller
{

    public function getAndDeleteProductByTags($products){

        $deleteProduct = new DeleteProduct();
        $deleteProduct->getAndDeleteProductByTags($products);

    }
}
