<?php


namespace App\Http\Controllers\product;

use App\domain\product\GetProduct;
use App\Http\Controllers\Controller;

class GetProductsController extends Controller
{
    public function getNewProductIdByProduct($products_ids_list)
    {
        $getProduct = new GetProduct();
        $getProduct->getNewProductIdByProduct($products_ids_list);
    }

    public function getSkuProductByProductId($product_id)
    {
        $getProduct = new GetProduct();
        $getProduct->getSkuProductByProductId($product_id);
    }
}
