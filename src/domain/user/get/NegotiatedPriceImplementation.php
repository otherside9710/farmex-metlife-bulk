<?php

namespace App\domain\user;

class NegotiatedPriceImplementation
{
    private $soapService;

    public function __construct()
    {
        $this->soapService = new SoapService();
    }

    public function getNegotiatedPrice($productsData){
        $this->soapService->getNegotiatedPriceSOAP($productsData);
    }
}
