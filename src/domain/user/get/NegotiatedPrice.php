<?php

namespace App\domain\user;

class NegotiatedPrice
{
    private $negotiatedPriceRepository;

    public function __construct()
    {
        $this->negotiatedPriceRepository = new NegotiatedPriceRepository();
    }

    public function getNegotiatedPrice($productsData){
        $this->negotiatedPriceRepository->getNegotiatedPrice($productsData);
    }
}
