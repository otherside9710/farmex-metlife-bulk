<?php

namespace App\domain\product;

class DeleteProduct
{
    private $deleteProductRepository;

    public function __construct()
    {
        $this->deleteProductRepository = new DeleteProductRepository();
    }

    public function getAndDeleteProductByTags($products){
        $this->deleteProductRepository->getAndDeleteProductByTags($products);
    }
}
