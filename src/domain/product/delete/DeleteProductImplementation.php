<?php

namespace App\domain\product;
use App\domain\user\ShopifyService;

class DeleteProductImplementation
{
    private $shopifyService;

    public function __construct()
    {
        $this->shopifyService = new ShopifyService();
    }

    public function getAndDeleteProductByTags($products){
        $this->shopifyService->getAndDeleteProductByTags($products);
    }
}
