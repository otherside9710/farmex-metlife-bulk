<?php

namespace App\domain\product;

class GetProductRepository extends GetProductImplementation
{

    public function getProduct($productId){
        parent::getProduct($productId);
    }

    public function getNewProductIdByProduct($products_ids_list){
        parent::getNewProductIdByProduct($products_ids_list);
    }

    public function getSkuProductByProductId($product_id){
        parent::getSkuProductByProductId($product_id);
    }
}
