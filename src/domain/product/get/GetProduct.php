<?php

namespace App\domain\product;

class GetProduct
{
    private $getProductRepository;

    public function __construct()
    {
        $this->getProductRepository = new GetProductRepository();
    }

    public function getProduct($productId){
        $this->getProductRepository->getProduct($productId);
    }

    public function getNewProductIdByProduct($products_ids_list){
        $this->getProductRepository->getNewProductIdByProduct($products_ids_list);
    }

    public function getSkuProductByProductId($product_id){
        $this->getProductRepository->getSkuProductByProductId($product_id);
    }
}
