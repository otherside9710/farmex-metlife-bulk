<?php

namespace App\domain\product;

use App\domain\user\ShopifyService;
use App\domain\user\SoapService;

class GetProductImplementation
{
    private $soapService;
    private $shopifyService;

    public function __construct()
    {
        $this->soapService = new SoapService();
        $this->shopifyService = new ShopifyService();
    }

    public function getProduct($productId){
       // $this->soapService->getProduct($productId);
    }

    public function getNewProductIdByProduct($productId){
        $this->shopifyService->getNewProductIdByProduct($productId);
    }

    public function getSkuProductByProductId($product_id){
        $this->shopifyService->getSkuProductByProductId($product_id);
    }
}
