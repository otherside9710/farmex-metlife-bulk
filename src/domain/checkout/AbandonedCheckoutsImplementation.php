<?php

namespace App\domain\checkout;
use App\domain\user\ShopifyService;

class AbandonedCheckoutsImplementation
{
    private $shopifyService;

    public function __construct()
    {
        $this->shopifyService = new ShopifyService();
    }

    public function abandonedCheckouts(){
        $this->shopifyService->deleteAbandonedCheckouts();
    }
}
