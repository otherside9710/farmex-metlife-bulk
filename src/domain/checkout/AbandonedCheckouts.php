<?php

namespace App\domain\checkout;

class AbandonedCheckouts
{
    private $abandonedCheckoutsRepository;

    public function __construct()
    {
        $this->abandonedCheckoutsRepository = new AbandonedCheckoutsRepository();
    }

    public function abandonedCheckouts(){
        $this->abandonedCheckoutsRepository->abandonedCheckouts();
    }
}
