<?php

namespace App\Exceptions\Shared;

use App\Exceptions\Handler;

class GenericHTTPExceptions extends Handler
{
    public function NOT_FOUND($message)
    {
        http_response_code(404);

        $json_response = [];
        $json_response['status'] = 404;
        $json_response['message'] = $message;

        $json_response = json_encode($json_response);

        header('Content-type: application/json');

        echo $json_response;

        die();
    }

    public function INTERNAL_SERVER_ERROR($error, $message)
    {
        http_response_code(500);

        $json_response = [];
        $json_response['status'] = 500;
        $json_response['error'] = $error;
        $json_response['message'] = $message;

        $json_response = json_encode($json_response);

        header('Content-type: application/json');

        echo $json_response;

        die();
    }

    public function INTERNAL_SERVER_ERROR_CUSTOM($error, $message, $data)
    {
        http_response_code(500);

        $json_response = [];
        $json_response['status'] = 500;
        $json_response['error'] = $error;
        $json_response['message'] = $message;
        $json_response['data'] = $data;

        $json_response = json_encode($json_response);

        header('Content-type: application/json');

        echo $json_response;

        die();
    }
}
